package com.example.sonarqubedemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SonarqubeDemoApplication

fun main(args: Array<String>) {
	runApplication<SonarqubeDemoApplication>(*args)
}
